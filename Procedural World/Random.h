#pragma once

#include <cstdlib>
#include <ostream>
#include "glm.hpp"
#include "gtc/random.hpp"

using namespace std;

template <typename type>
class UniformDistribution
{
public:
	UniformDistribution();
	UniformDistribution( const type& min, const type& max );
	~UniformDistribution();

	type value;

	type & Sample();

	operator type() { return Sample(); }

private:
	type offset;
	type range;
};

template<typename type>
UniformDistribution<type>::UniformDistribution()
{}

template<typename type>
inline UniformDistribution<type>::UniformDistribution( const type & min, const type & max ) : offset( min ), range( max - min )
{}

template<typename type>
UniformDistribution<type>::~UniformDistribution()
{}

template<typename type>
type & UniformDistribution<type>::Sample()
{
	return value = offset + static_cast <type> (rand()) / (static_cast <type> (RAND_MAX / range));
}

template <typename type>
ostream& operator << (ostream& stream, UniformDistribution<type> & rand)
{
	return stream << (type) rand;
}

typedef UniformDistribution<float> rand_float;
typedef UniformDistribution<int> rand_int;

static rand_float unit( 0.0f, 1.0f );

class GaussianDistribution
{
public:
	const float min;
	const float max;

	const float mean;
	const float std;

	float Sample()
	{
		float f;
			
		f = mean + glm::gaussRand( mean, std ) * std;

		return f;
	}

	GaussianDistribution( const float & min, const float & max, const float & deviations = 3.5f ) : min( min ), max( max ), mean( (min + max) /2.0f ), std( abs( max - min ) / deviations ) {}
	~GaussianDistribution() {}
};

