#pragma once

#include "SceneObject.h"
#include "HasTransform.h"
#include "HasMesh.h"
#include "HasColor.h"

class Drawable : public HasTransform, public HasMesh, public HasColor
{
public:
	Drawable();
	virtual ~Drawable();
};

