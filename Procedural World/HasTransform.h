#pragma once

#include <memory>

#include "Transform.h"

using namespace std;

class HasTransform
{
public:
	virtual ~HasTransform();

	Transform transform;

protected:
	HasTransform();
};

