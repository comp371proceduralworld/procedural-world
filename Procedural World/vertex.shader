#version 430 core

layout(location = 0) in vec3 position;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec2 uv;
layout(location = 3) in vec3 t;
layout(location = 4) in vec3 bt;

out vec3 vNormal;
out vec3 vT;
out vec3 vBT;
out vec2 vUV;

void main()
{
	//Every coordinate change is done in the T.E.S.
	gl_Position = vec4(position, 1.0);
	vUV = uv;
	vT = t;
	vBT = bt;
	vNormal = normal;
}