#pragma once

#include "..\glm\glm.hpp"

class Material
{
public:
	Material();
	~Material();
	
	void setDiffuseCoefficient( const glm::vec4& color );
	void setSpecularCoefficient( const glm::vec4& color );
	void setSpecularExponent( const float& coefficient );

	glm::vec4 getDiffuseCoefficient() const;
	glm::vec4 getSpecularCoefficient() const;
	float getSpecularExponent() const;

private:
	glm::vec4 diffuse_coefficient;
	glm::vec4 specular_coefficient;
	float specular_exponent;
};

