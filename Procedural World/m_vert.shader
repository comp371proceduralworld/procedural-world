#version 430 core

layout(location = 0) in vec3 position;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec2 uv;

out vec3 vOriPos;
out vec3 vNormal;
out vec2 vUV;

void main()
{
	vOriPos = position;
	gl_Position = vec4(position, 1.0);
	vNormal = normal;
	vUV = uv;
}