#include "Camera.h"

#include <iostream>

#include "..\glm\gtc\matrix_transform.inl"
#include "..\glm\gtc\type_ptr.hpp"

Camera::Camera() : z_near( DEFAULT_NEAR_CLIP_PLANE_DISTNACE ), z_far( DEFAULT_FAR_CLIP_PLANE_DISTNACE ), projection( 1.0f ), clear_color( 0.0f, 0.0f, 0.0f, 1.0f ), clear_mask( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT )
{
	cout << "Camera()\n";
}

void Camera::preRender( Window *& window )
{
	glViewport( 0, 0, window->getWidth(), window->getHeight() );

	calculateProjectionMatrix( window );

	glClearColor( clear_color.r, clear_color.g, clear_color.b, clear_color.a );

	glClear( clear_mask );
}

Camera::~Camera()
{
	cout << "~Camera()\n";
}

void Camera::setFarClipPlaneDistance( float distance )
{
	z_far = distance < 0.0f ? 0.0f : distance;
}

void Camera::setNearClipPlaneDistance( float distance )
{
	z_near = distance < 0.0f ? 0.0f : distance;
}

float Camera::getFarClipPlaneDistance() const
{
	return z_far;
}

float Camera::getNearClipPlaneDistance() const
{
	return z_near;
}

void Camera::enableColorClear()
{
	clear_mask |= GL_COLOR_BUFFER_BIT;
}

void Camera::disableColorClear()
{
	clear_mask &= ~GL_COLOR_BUFFER_BIT;
}

bool Camera::isColorClearEnabled()
{
	return (clear_mask & GL_COLOR_BUFFER_BIT) != GL_NONE;
}

void Camera::enableDepthClear()
{
	clear_mask |= GL_DEPTH_BUFFER_BIT;
}

void Camera::disableDepthClear()
{
	clear_mask &= ~GL_DEPTH_BUFFER_BIT;
}

bool Camera::isDepthClearEnabled()
{
	return (clear_mask & GL_DEPTH_BUFFER_BIT) != GL_NONE;
}

void Camera::setClearColor( const glm::vec4& color )
{
	clear_color = color;
}

glm::vec4 Camera::getClearColor() const
{
	return clear_color;
}

void Camera::render( const Batch*& batch, Window *& window, GLuint program_ID, GLuint vao_ID )
{
	preRender( window );

	//TODO:
	//render scene objects, batched by material (ShaderProgram)

	//see http://www.openglsuperbible.com/2013/10/16/the-road-to-one-million-draws/

	//see https://www.opengl.org/sdk/docs/man/html/glMultiDrawArraysIndirect.xhtml
}

void Camera::debug( vector<Instance*>& instances, Window *& window, GLuint program_ID, GLuint vao_ID )
{
	preRender( window );

	GLuint model_patrix_location = glGetUniformLocation( program_ID, "model_matrix" );

	glUseProgram( program_ID );

	glBindVertexArray( vao_ID );

	for ( auto instance : instances )
	{
		glEnableVertexAttribArray( instance->getMesh()->getVertexBufferID() );

		if ( instance->getMesh()->hasColorData() )
		{
			glEnableVertexAttribArray( instance->getMesh()->getColorBufferID() );
		}

		if ( instance->getMesh()->hasNormalData() )
		{
			glEnableVertexAttribArray( instance->getMesh()->getNormalBufferID() );
		}

		if ( instance->getMesh()->hasTangentData() )
		{
			glEnableVertexAttribArray( instance->getMesh()->getTangentBufferID() );
		}

		if ( instance->getMesh()->hasUVData() )
		{
			glEnableVertexAttribArray( instance->getMesh()->getUVBufferID() );
		}

		glUniformMatrix4fv( model_patrix_location, 1, GL_FALSE, glm::value_ptr( instance->transform.getMatrix() ) );

		glDrawElements( GL_TRIANGLES, instance->getMesh()->getTriangleCount() * 3, GL_UNSIGNED_INT, instance->getMesh()->getTriangleData() );
		
		glDisableVertexAttribArray( instance->getMesh()->getVertexBufferID() );

		if ( instance->getMesh()->hasColorData() )
		{
			glDisableVertexAttribArray( instance->getMesh()->getColorBufferID() );
		}

		if ( instance->getMesh()->hasNormalData() )
		{
			glDisableVertexAttribArray( instance->getMesh()->getNormalBufferID() );
		}

		if ( instance->getMesh()->hasTangentData() )
		{
			glDisableVertexAttribArray( instance->getMesh()->getTangentBufferID() );
		}

		if ( instance->getMesh()->hasUVData() )
		{
			glDisableVertexAttribArray( instance->getMesh()->getUVBufferID() );
		}
	}

	glBindVertexArray( 0 );

	glUseProgram( 0 );
}

OrthographicCamera::OrthographicCamera() : size_y( DEFAULT_ORTHOGRAPHIC_SIZE )
{}

OrthographicCamera::OrthographicCamera( float size ) : size_y( size )
{}

OrthographicCamera::~OrthographicCamera()
{}

void OrthographicCamera::setVerticalSize( float size )
{
	size_y = size;
}

float OrthographicCamera::getVerticalSize()
{
	return size_y;
}

void OrthographicCamera::calculateProjectionMatrix( Window*& window )
{
	float half_height = size_y / 2.0f;
	float half_width = half_height * window->getAspectRatio();
	
	projection = glm::ortho( -half_width, half_width, -half_height, half_height );
}

PerspectiveCamera::PerspectiveCamera() : fov_y( DEFAULT_FOV )
{}

PerspectiveCamera::PerspectiveCamera( float fov ) : fov_y( fov )
{}

PerspectiveCamera::~PerspectiveCamera()
{}

void PerspectiveCamera::setVerticalFOV( float fov )
{
	fov_y = fov;
}

float PerspectiveCamera::getVerticalFOV()
{
	return fov_y;
}

void PerspectiveCamera::calculateProjectionMatrix( Window*& window )
{
	projection = glm::perspective( fov_y, window->getAspectRatio(), z_near, z_far );
}
