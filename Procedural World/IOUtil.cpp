#include "IOUtil.h"

#include <string>;
#include <fstream>;

const char* readFileContents( const char * filename )
{
	std::string* contents = new std::string();

	std::ifstream filestream( filename, std::ios::in );

	if ( filestream.is_open() )
	{
		std::string next = "";

		while ( getline( filestream, next ) )
		{
			*contents += "\n" + next;
		}

		filestream.close();
	}

	return contents->data();
}