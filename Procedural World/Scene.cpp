#include "Scene.h"

#include <iostream>

using namespace std;

Scene::Scene()
{
	cout << "Scene()\n";
}


Scene::~Scene()
{
	cout << "~Scene()\n";
}

void Scene::update()
{
	for ( auto updateComponent : updateComponents )
	{
		updateComponent->update();
	}
}

void Scene::addCamera( Camera * camera )
{
	cameras.push_back( camera );
}

void Scene::addInstance( Instance * instance )
{
	instances.push_back( instance );
}

void Scene::addUpdateReceiver( CanUpdate * receiver )
{
	updateComponents.push_back( receiver );
}

void Scene::renderWith( Camera * camera, Window * window, const GLuint & program_id, const GLuint & vao_id )
{
	camera->debug( instances, window, program_id, vao_id );
}
