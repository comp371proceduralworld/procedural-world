#version 430 core

layout(triangles, equal_spacing, ccw) in;

in vec3 tcsOriPos[];
out vec3 tesOriPos;

in vec3 tcsNormal[];
in vec2 tcsUV[];

uniform mat4 model_matrix;
uniform mat4 view_matrix;
uniform mat4 projection_matrix;

vec2 interpolate2D(vec2 v0, vec2 v1, vec2 v2)
{
	return vec2(gl_TessCoord.x) * v0 + vec2(gl_TessCoord.y) * v1 + vec2(gl_TessCoord.z) * v2;
}

vec3 interpolate3D(vec3 v0, vec3 v1, vec3 v2)
{
	return vec3(gl_TessCoord.x) * v0 + vec3(gl_TessCoord.y) * v1 + vec3(gl_TessCoord.z) * v2;
}

uniform sampler2D noiseTexture;

void main(void) {
	vec4 pos = vec4(interpolate3D(gl_in[0].gl_Position.xyz, gl_in[1].gl_Position.xyz, gl_in[2].gl_Position.xyz), 1.0);
	vec3 norm = interpolate3D(tcsNormal[0],tcsNormal[1],tcsNormal[2]);
	vec2 uv = interpolate2D(tcsUV[0],tcsUV[1],tcsUV[2]);

	float height = texture(noiseTexture, uv).x;
	pos += vec4(norm,1.0) * (height * 0.1f);
	tesOriPos = interpolate3D(tcsOriPos[0], tcsOriPos[1], tcsOriPos[2]);
	gl_Position = projection_matrix * view_matrix * model_matrix * pos;
}