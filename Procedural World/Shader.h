/*
Author : Samuel Beland-Leblanc(27185642)
Purpose : Helper class to load the vertex and fragment shader from file, compile them and link them
*/
#pragma once
#include<string>
#include "glew.h"

using namespace std;

class Shader
{
private:

	GLuint _current_program;

	GLuint LoadAndCompileSingle(GLenum type, std::string filename);
	bool AttachAndLinkProgram(GLuint shaders[], int numOfvectors);

public:
	Shader();
	~Shader();

	bool LoadAndCompileShaders(string vertex, string fragment);
	bool LoadAndCompileShaders(string vertex, string fragment, string tcs, string tes);
	bool LoadAndCompileShaders(string vertex, string fragment, string tcs, string tes, string gs);

	GLuint GetCurrentProgram() { return _current_program; }
};

