#include "Material.h"

#include <iostream>

using namespace std;

Material::Material()
{
	cout << "Material()\n";
}

Material::~Material()
{
	cout << "~Material()\n";
}

void Material::setDiffuseCoefficient( const glm::vec4 & color )
{
	diffuse_coefficient = color;
}

void Material::setSpecularCoefficient( const glm::vec4 & color )
{
	specular_coefficient = color;
}

void Material::setSpecularExponent( const float & coefficient )
{}

glm::vec4 Material::getDiffuseCoefficient() const
{
	return diffuse_coefficient;
}

glm::vec4 Material::getSpecularCoefficient() const
{
	return specular_coefficient;
}

float Material::getSpecularExponent() const
{
	return specular_exponent;
}
