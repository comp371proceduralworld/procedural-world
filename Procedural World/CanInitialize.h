#pragma once
class CanInitialize
{
public:
	~CanInitialize();
	virtual void initialize() = 0;

protected:
	CanInitialize();
};

