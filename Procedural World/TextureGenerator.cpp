#include "TextureGenerator.h"
#include <stdlib.h>
#include "ArrayHelper.h"

TextureGenerator::TextureGenerator(int width, int height, float horizontalFactor, float verticalFactor, float turbPower, int edgeOverlap) : _height(height), _width(width)
{
	float** noise = ArrayHelper::Generate2DArray<float>(width, height);
	float** smoothNoise = ArrayHelper::Generate2DArray<float>(width, height);
	_data = new glm::vec3[width * height];
	_normals = new glm::vec3[width * height];

	//Generate basic noise
	for (int i = 0; i < height; ++i) {
		for (int j = 0; j < width; ++j) {
			noise[i][j] = (float)rand() / RAND_MAX;
		}
	}

	//Initialize smoothNoise
	for (int i = 0; i < height; ++i) {
		for (int j = 0; j < width; ++j) {
			smoothNoise[i][j] = 0.0f;
		}
	}

	//Generate smooth noise over 5 octaves
	for (int k = 1; k <= 32; k *= 2) {
		for (int i = 0; i < height; ++i) {
			for (int j = 0; j < width; ++j) {
				double x = i / (double)k;
				double y = j / (double)k;

				double fracX = x - int(x);
				double fracY = y - int(y);

				int x1 = (int(x) + width) % width;
				int y1 = (int(y) + height) % height;

				int x2 = (x1 + width - 1) % width;
				int y2 = (y1 + height - 1) % height;

				double value = 0.0;
				value += fracX * fracY * noise[y1][x1];
				value += (1 - fracX) * fracY * noise[y1][x2];
				value += fracX * (1 - fracY) * noise[y2][x1];
				value += (1 - fracX) * (1 - fracY) * noise[y2][x2];

				smoothNoise[i][j] += (k / 32.0f) *value;
			}
		}
	}

	//Generate the actual texture
	for (int i = 0; i < height; ++i) {
		for (int j = 0; j < width; ++j) {
			float noiseImpact = j*verticalFactor / width + i * horizontalFactor / height + turbPower * smoothNoise[i][j];
			float val = 1 - fabs(sin(noiseImpact * 3.14159f));
			_data[width * i + j] = glm::vec3(val, val, val);
		}
	}

	//Generate the normals for the texture
	for (float y = 0; y < height; ++y) {
		for (float x = 0; x < width; ++x) {
			float nextX, nextY, prevX, prevY;
			if (x == 0) {
				prevX = width - edgeOverlap;
			}
			else {
				prevX = x - 1;
			}
			if (y == 0) {
				prevY = height - edgeOverlap;
			}
			else {
				prevY = y - 1;
			}
			if (x == width - 1) {
				nextX = edgeOverlap;
			}
			else {
				nextX = x + 1;
			}
			if (y == height - 1) {
				nextY = edgeOverlap;
			}
			else {
				nextY = y + 1;
			}
			glm::vec3 current(x / width, y / height, _data[(int)y*width + (int)x].r);
			glm::vec3 nextXPoint(nextX / width, y / height, _data[(int)y*width + (int)nextX].r);
			glm::vec3 nextYPoint(x / width, nextY / height, _data[(int)nextY*width + (int)x].r);
			glm::vec3 prevXPoint(prevX / width, y / height, _data[(int)y*width + (int)prevX].r);
			glm::vec3 prevYPoint(x / width, prevY / height, _data[(int)prevY*width + (int)x].r);

			float z1 = nextXPoint.z - prevXPoint.z;
			float z2 = nextYPoint.z - prevYPoint.z;

			z1 *= 3;
			z2 *= 3;

			glm::vec3 A(1, 0, z1);
			glm::vec3 B(0, 1, z2);
			// + 1 / 2
			glm::vec3 v1 = nextXPoint - prevXPoint;
			glm::vec3 v2 = nextYPoint - prevYPoint;
			glm::vec3 oldValue = glm::normalize(glm::cross(A, B));
			_normals[(int)y*width + (int)x] = (oldValue + 1.0f) / 2.0f;
		}
	}

	ArrayHelper::Delete2DArray<float>(noise, height);
	ArrayHelper::Delete2DArray<float>(smoothNoise, height);
}
	

TextureGenerator::~TextureGenerator()
{
	delete[] _data;
	delete[] _normals;
}

