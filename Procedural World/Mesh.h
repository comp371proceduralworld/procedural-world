#pragma once

#include <tuple>
#include <glew.h>
#include <glfw3.h>
#include "..\glm\glm.hpp"

class Mesh
{
public:
	Mesh( const unsigned int & vertices, const unsigned int & triangles );
	~Mesh();

	void push( GLuint vao_ID );
	void pop();
	void sub();

	GLuint getVertexBufferID() const;
	GLuint getElementBufferID() const;
	GLuint getColorBufferID() const;
	GLuint getNormalBufferID() const;
	GLuint getTangentBufferID() const;
	GLuint getUVBufferID() const;

	void setVertex( int i, glm::vec3 position );
	glm::vec3 getVertex( int i ) const;

	void setTriangle( int i, unsigned int a, unsigned int b, unsigned int c );
	std::tuple<unsigned int, unsigned int, unsigned int> getTriangle( int i ) const;

	void setColor( int i, glm::vec3 color );
	glm::vec3 getColor( int i ) const;

	void setNormal( int i, glm::vec3 vector );
	glm::vec3 getNormal( int i ) const;

	void setTangent( int i, glm::vec3 vector );
	glm::vec3 getTangent( int i ) const;

	void setBitangent( int i, glm::vec3 vector );
	glm::vec3 getBitangent( int i ) const;

	void setUV( int i, glm::vec2 coordinate );
	glm::vec2 getUV( int i ) const;

	unsigned int getVertexCount() const;
	unsigned int getTriangleCount() const;

	void addColorData();
	bool hasColorData() const;

	void addNormalData();
	bool hasNormalData() const;

	void addTangentData();
	bool hasTangentData() const;

	void addBiangentData();
	bool hasBiangentData() const;

	void addUVData();
	bool hasUVData() const;

	unsigned int * getTriangleData() const;

	float * getVertexData() const;
	float * getColorData() const;
	float * getNormalData() const;
	float * getTangentData() const;
	float * getBitangentData() const;
	float * getUVData() const;

private:
	unsigned int vertices;
	unsigned int triangles;

	GLuint vao_ID;

	GLuint position_vbo_ID;
	GLuint color_vbo_ID;
	GLuint normal_vbo_ID;
	GLuint tangent_vbo_ID;
	GLuint bitangent_vbo_ID;
	GLuint uv_vbo_ID;

	GLuint ebo_ID;
	
	float * position_data;
	float * color_data;
	float * normal_data;
	float * tangent_data;
	float * bitangent_data;
	float * uv_data;
	
	unsigned int * triangle_data;
};

