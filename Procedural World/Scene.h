#pragma once

#include <map>
#include <vector>
#include <memory>

#include "CanInitialize.h"
#include "CanUpdate.h"
#include "Camera.h"

using namespace std;

class Scene : public CanUpdate
{
public:
	Scene();
	~Scene();
	void update();
	void addCamera( Camera* camera );
	void addInstance( Instance* instance );

	void addUpdateReceiver( CanUpdate* receiver );

	void renderWith( Camera* camera, Window * window, const GLuint & program_id, const GLuint & vao_id );

private:
	vector<Instance*> instances;
	vector<Camera*> cameras;

	vector<CanUpdate*> updateComponents;
};

