/*
Author : Samuel Beland-Leblanc(27185642)
Purpose : Helper class to load the vertex and fragment shader from file, compile them and link them
*/

#include "Shader.h"
#include <fstream>
#include <iostream>
#include <sstream>

GLuint Shader::LoadAndCompileSingle(GLenum type, std::string filename)
{
	string content;
	ifstream vsF(filename.c_str(), ifstream::in);

	if (vsF.is_open()) {

		stringstream vsS, fsS;
		vsS << vsF.rdbuf();

		content = vsS.str();
	}
	else {
		return 0;
	}
	vsF.close();
	GLint success;
	GLchar log[512];
	GLuint shader;

	const GLchar* finalContent = (const GLchar*)content.c_str();
	shader = glCreateShader(type);
	glShaderSource(shader, 1, &finalContent, NULL);
	glCompileShader(shader);
	glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
	if (!success) {
		glGetShaderInfoLog(shader, 512, NULL, log);
		std::cout << "ERROR::SHADER::"<< filename <<"::COMPILATION_FAILED\n" << log << std::endl;
		return 0;
	}

	return shader;
}

bool Shader::AttachAndLinkProgram(GLuint shaders[], int numOfvectors)
{
	GLint success;
	GLchar log[512];
	_current_program = glCreateProgram();
	for (int i = 0; i < numOfvectors; ++i) {
		glAttachShader(_current_program, shaders[i]);
	}
	
	glLinkProgram(_current_program);

	glGetProgramiv(_current_program, GL_LINK_STATUS, &success);
	if (!success)
	{
		glGetProgramInfoLog(_current_program, 512, NULL, log);
		std::cout << "ERROR::SHADER::PROGRAM::LINKING_FAILED\n" << log << std::endl;
		return false;
	}

	//Delete the shaders now that they are linked
	for (int i = 0; i < numOfvectors; ++i) {
		glDeleteShader(shaders[i]);
	}
	return true;
}

Shader::Shader()
{
	_current_program = 0;
}


Shader::~Shader()
{
	glDeleteProgram(_current_program);
}

//Load the shaders' source from file, compile them and links them to a new program
bool Shader::LoadAndCompileShaders(string vertex, string fragment)
{
	GLuint shaders[2];

	shaders[0] = LoadAndCompileSingle(GL_VERTEX_SHADER, vertex);
	if (shaders[0] == 0) return false;

	shaders[1] = LoadAndCompileSingle(GL_FRAGMENT_SHADER, fragment);
	if (shaders[1] == 0) return false;

	return AttachAndLinkProgram(shaders, 2);
}


bool Shader::LoadAndCompileShaders(string vertex, string fragment, string tcs, string tes)
{
	GLuint shaders[4];

	shaders[0] = LoadAndCompileSingle(GL_VERTEX_SHADER, vertex);
	if (shaders[0] == 0) return false;

	shaders[1] = LoadAndCompileSingle(GL_FRAGMENT_SHADER, fragment);
	if (shaders[1] == 0) return false;

	shaders[2] = LoadAndCompileSingle(GL_TESS_CONTROL_SHADER, tcs);
	if (shaders[2] == 0) return false;

	shaders[3] = LoadAndCompileSingle(GL_TESS_EVALUATION_SHADER, tes);
	if (shaders[3] == 0) return false;

	return AttachAndLinkProgram(shaders, 4);
}

bool Shader::LoadAndCompileShaders(string vertex, string fragment, string tcs, string tes, string gs)
{
	GLuint shaders[5];

	shaders[0] = LoadAndCompileSingle(GL_VERTEX_SHADER, vertex);
	if (shaders[0] == 0) return false;

	shaders[1] = LoadAndCompileSingle(GL_FRAGMENT_SHADER, fragment);
	if (shaders[1] == 0) return false;

	shaders[2] = LoadAndCompileSingle(GL_TESS_CONTROL_SHADER, tcs);
	if (shaders[2] == 0) return false;

	shaders[3] = LoadAndCompileSingle(GL_TESS_EVALUATION_SHADER, tes);
	if (shaders[3] == 0) return false;

	shaders[4] = LoadAndCompileSingle(GL_GEOMETRY_SHADER, gs);
	if (shaders[4] == 0) return false;

	return AttachAndLinkProgram(shaders, 5);
}