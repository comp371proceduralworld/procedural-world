#pragma once

#include <iostream>
#include <../glm/glm.hpp>

using namespace std;
using namespace glm;

inline ostream& operator << ( ostream& stream, const vec2& vec )
{
	return stream << '(' << vec.x << ", " << vec.y << ')' << endl;
}

inline ostream& operator << ( ostream& stream, const vec3& vec )
{
	return stream << '(' << vec.x << ", " << vec.y << ", " << vec.z << ')' << endl;
}

inline ostream& operator << ( ostream& stream, const vec4& vec )
{
	return stream << '(' << vec.x << ", " << vec.y << ", " << vec.z << vec.w << ')' << endl;
}