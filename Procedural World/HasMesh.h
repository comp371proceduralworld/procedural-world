#pragma once

#include "Mesh.h"

using namespace std;

class HasMesh
{
public:
	virtual ~HasMesh();

	Mesh* getMesh() const;
	void setMesh( Mesh* mesh );

protected:
	HasMesh();

	Mesh* mesh;
};

