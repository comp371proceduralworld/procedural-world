#pragma once

#include <glew.h>
#include <glfw3.h>

static double t;

static float currentTime;
static unsigned long currentTimeMilliseconds;
static float deltaTime;
static unsigned long deltaTimeMilliseconds;
static unsigned int fps;

class Time
{
private:
	Time();
	~Time();
public:
	static float getCurrentTime()
	{
		return currentTime;
	}

	static float getDeltaTime()
	{
		return deltaTime;
	}
	static unsigned long getCurrentTimeMilliseconds()
	{
		return currentTimeMilliseconds;
	}

	static unsigned long getDeltaTimeMilliseconds()
	{
		return deltaTimeMilliseconds;
	}

	static unsigned int getFPS()
	{
		return fps;
	}

	static void update()
	{
		t = glfwGetTime();

		float tf = (float) t;

		deltaTime = (float) tf - currentTime;
		deltaTimeMilliseconds = (unsigned long) (deltaTime * 1000);

		currentTime = (float) tf;
		currentTimeMilliseconds = (unsigned long) (currentTime * 1000);

		fps = 1.0f / deltaTime;
	}
};

