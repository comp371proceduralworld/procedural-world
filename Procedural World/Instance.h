#pragma once

#include "SceneObject.h"
#include "Drawable.h"

class Instance : public SceneObject, public Drawable
{
public:
	Instance();
	virtual ~Instance();
};

