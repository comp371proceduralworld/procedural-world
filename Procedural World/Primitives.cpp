#include "Primitives.h"

// golden ratio denominator
const float phi = (1.0f + sqrtf( 5.0f )) / 2.0f;

Primitives::Primitives()
{}

Primitives::~Primitives()
{}

Mesh * Primitives::Icosahedron( const unsigned int & subdivisions )
{
	unsigned int triangles = 20;
	unsigned int vertices = 12;

	for ( unsigned int i = 0; i < subdivisions; i++ )
	{
		vertices += triangles;
		triangles *= 3;
	}

	Mesh * mesh = new Mesh( vertices, triangles );

	return mesh;
}

Mesh * Primitives::Cube( const unsigned int & subdivisions )
{
	return nullptr;
}

Mesh * Primitives::Plane( const unsigned int & subdivisions )
{
	unsigned int triangles = 2;
	unsigned int side = 1;

	for ( unsigned int i = 0; i < subdivisions; i++ )
	{
		triangles *= 4;
		side *= 2;
	}

	side += 1;

	unsigned int vertices = side * side;

	Mesh * mesh = new Mesh( vertices, triangles );

	mesh->addNormalData();
	//mesh->addTangentData();
	//mesh->addBiangentData();
	mesh->addUVData();

	int i = 0;

	for ( unsigned int x = 0; x < side; x++ )
	{
		for ( unsigned int y = 0; y < side; y++ )
		{
			mesh->setNormal( i, glm::vec3( 0, 0, 1 ) );
			mesh->setUV( i, glm::vec2( x / (side - 1.0f), y / (side - 1.0f) ) );
		}
	}

	return mesh;
}
