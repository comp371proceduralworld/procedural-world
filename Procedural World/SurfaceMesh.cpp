#include "SurfaceMesh.h"

void SurfaceMesh::CalculateNormalsFor( SubdivisionSurface * surface )
{
	// accumulate normals from each face
	for ( auto face : surface->faces )
	{
		face->vertices[0]->normal += cross( (face->vertices[face->sides - 1]->position - face->vertices[0]->position), (face->vertices[1]->position - face->vertices[0]->position) );

		for ( int i = 1; i < face->sides - 1; i++ )
		{
			face->vertices[i]->normal += cross( (face->vertices[i - 1]->position - face->vertices[i]->position), (face->vertices[i + 1]->position - face->vertices[i]->position) );
		}

		face->vertices[face->sides - 1]->normal += cross( (face->vertices[face->sides - 2]->position - face->vertices[face->sides - 1]->position), (face->vertices[0]->position - face->vertices[face->sides - 1]->position) );
	}

	// normalize the sum
	for ( auto vertex : surface->verts )
	{
		vertex->normal = normalize( vertex->normal );
	}
}

Mesh * SurfaceMesh::CreateMeshFrom( SubdivisionSurface * surface )
{
	Mesh * mesh = new Mesh( surface->verts.size(), surface->n_quad * 2 + surface->n_tris );

	mesh->addNormalData();
	mesh->addTangentData();
	mesh->addBiangentData();
	mesh->addUVData();

	// 1-to-1 correspondence
	float * vertices = mesh->getVertexData();
	float * normals = mesh->getNormalData();
	float * tangents = mesh->getTangentData();
	float * bitangents = mesh->getBitangentData();
	float * uv = mesh->getUVData();

	for ( int i = 0; i < surface->verts.size(); i++ )
	{
		mesh->setVertex( i, surface->verts[i]->position );
		mesh->setNormal( i, surface->verts[i]->normal );
		mesh->setTangent( i, surface->verts[i]->tangent );
		mesh->setBitangent( i, surface->verts[i]->bitangent );
		mesh->setUV( i, surface->verts[i]->uv );

		surface->verts[i]->order = i;
	}

	for ( int i = 0, j = 0; i < surface->faces.size(); i++ )
	{
		mesh->setTriangle( j++, surface->faces[i]->vertices[2]->order, surface->faces[i]->vertices[1]->order, surface->faces[i]->vertices[0]->order );
		mesh->setTriangle( j++, surface->faces[i]->vertices[3]->order, surface->faces[i]->vertices[2]->order, surface->faces[i]->vertices[0]->order );
	}

	return mesh;
}

SurfaceMesh::SurfaceMesh()
{}


SurfaceMesh::~SurfaceMesh()
{}
