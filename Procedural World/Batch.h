#pragma once

#include <vector>

#include "Instance.h"

class Batch
{
public:
	Batch( vector<Instance*>* instances );
	~Batch();
};

