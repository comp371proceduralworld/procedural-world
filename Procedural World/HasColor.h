#pragma once

#include "..\glm\glm.hpp"

class HasColor
{
public:
	virtual ~HasColor();

	void setColor( const glm::vec4 & color );
	const glm::vec4 & getColor();

protected:
	HasColor();

	glm::vec4 color;
};

