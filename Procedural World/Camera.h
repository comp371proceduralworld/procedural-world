#pragma once

#include "..\glm\glm.hpp"

#include <memory>

#include "SceneObject.h"
#include "Batch.h"
#include "Window.h"

class Camera : public SceneObject, public HasTransform
{
public:
	~Camera();

	static constexpr float DEFAULT_NEAR_CLIP_PLANE_DISTNACE = 0.05f;
	static constexpr float DEFAULT_FAR_CLIP_PLANE_DISTNACE = 10000.0f;

	void setFarClipPlaneDistance( float distance );
	void setNearClipPlaneDistance( float distance );
	float getFarClipPlaneDistance() const;
	float getNearClipPlaneDistance() const;

	void enableColorClear();
	void disableColorClear();
	bool isColorClearEnabled();
	void enableDepthClear();
	void disableDepthClear();
	bool isDepthClearEnabled();

	void setClearColor( const glm::vec4& color );
	glm::vec4 getClearColor() const;

	void render( const Batch*& batch, Window*& window, GLuint program_ID, GLuint vao_ID );
	void debug( vector<Instance*>& instances, Window*& window, GLuint program_ID, GLuint vao_ID );

protected:
	Camera();
	virtual void calculateProjectionMatrix( Window*& window ) = 0;
	glm::mat4 projection;
	float z_near;
	float z_far;
private:
	void preRender( Window*& window );
	glm::vec4 clear_color;
	GLbitfield clear_mask;
};

class OrthographicCamera : public Camera
{
public:
	OrthographicCamera();
	OrthographicCamera( float size );
	~OrthographicCamera();

	static constexpr float DEFAULT_ORTHOGRAPHIC_SIZE = 1.0f;

	void setVerticalSize( float size );
	float getVerticalSize();

protected:
	virtual void calculateProjectionMatrix( Window*& window );

private:
	float size_y;
};

class PerspectiveCamera : public Camera
{
public:
	PerspectiveCamera();
	PerspectiveCamera( float fov );
	~PerspectiveCamera();

	static constexpr float DEFAULT_FOV = 45.0f;

	void setVerticalFOV( float fov );
	float getVerticalFOV();

protected:
	virtual void calculateProjectionMatrix( Window*& window );

private:
	float fov_y;
};

