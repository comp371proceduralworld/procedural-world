#include "HasColor.h"

HasColor::HasColor()
{
	color = glm::vec4( 1.0f );
}

HasColor::~HasColor()
{}

void HasColor::setColor( const glm::vec4 & color )
{
	this->color = color;
}

const glm::vec4 & HasColor::getColor()
{
	return color;
}
