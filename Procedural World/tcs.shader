#version 430 core

layout(vertices = 3) out;

in vec3 vNormal[];
out vec3 tcsNormal[];

in vec2 vUV[];
out vec2 tcsUV[];

in vec3 vT[];
out vec3 tcsT[];

in vec3 vBT[];
out vec3 tcsBT[];

const float TESS_LEVEL = 100.0;

void main(void) {
	if (gl_InvocationID == 0) {
		gl_TessLevelInner[0] = TESS_LEVEL;
		gl_TessLevelOuter[0] = TESS_LEVEL;
		gl_TessLevelOuter[1] = TESS_LEVEL;
		gl_TessLevelOuter[2] = TESS_LEVEL;
	}
	gl_out[gl_InvocationID].gl_Position = gl_in[gl_InvocationID].gl_Position;
	tcsNormal[gl_InvocationID] = vNormal[gl_InvocationID];
	tcsT[gl_InvocationID] = vT[gl_InvocationID];
	tcsBT[gl_InvocationID] = vBT[gl_InvocationID];
	tcsUV[gl_InvocationID] = vUV[gl_InvocationID];
}