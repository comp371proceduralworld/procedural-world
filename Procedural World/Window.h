#pragma once

#include "GLUtil.h"
#include <vector>

class Window
{
	friend void windowResizeCallback( GLFWwindow* window, int width, int height );
public:
	static constexpr int DEFAULT_WIDTH = 640;
	static constexpr int DEFAULT_HEIGHT = 480;

	Window();
	Window( int width, int height, const char* title, bool locked );
	~Window();

	int getWidth() const;
	int getHeight() const;
	float getAspectRatio() const;

	void setWidth( const int & width );
	void setHeight( const int & height );
	void setTitle( const char* title );
	void lock();
	void unlock();
	void showCursor();
	void hideCursor();

	void setKeyCallback( GLFWkeyfun callback );
	void setCursorPositionCallback( GLFWcursorposfun callback );
	void setMouseButtonCallback( GLFWmousebuttonfun callback );
	void setScrollWheelCallback( GLFWscrollfun callback );

	void swapBuffers();

	bool shouldClose();

private:
	GLFWwindow* window;
	int width;
	int height;
	float aspectRatio;
	bool locked;

	void handleResize( int width, int height );
};

static std::vector<Window*> windows = std::vector<Window*>();

static void windowResizeCallback( GLFWwindow* window, int width, int height );

