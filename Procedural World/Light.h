#pragma once

#include "..\glm\glm.hpp"

#include "SceneObject.h"
#include "HasTransform.h"
#include "HasColor.h"

class Light : public SceneObject, public HasColor
{
public:
	~Light();

protected:
	Light();
};

class ShadowCastingLight
{
public:
	~ShadowCastingLight();

	void enableShadows();
	void disableShadows();

	bool castsShadows();

protected:
	ShadowCastingLight();

private:
	bool enabled;
};

class AmbientLight : public Light
{
public:
	AmbientLight();
	~AmbientLight();
};

class DirectionalLight : public ShadowCastingLight
{
public:
	DirectionalLight();
	~DirectionalLight();
	
	void setDirection( glm::vec3 vector );
	glm::vec3 getDirection();

private:
	glm::vec3 direction;
};

class PointLight : public ShadowCastingLight, public HasTransform
{
public:
	PointLight();
	~PointLight();
};
