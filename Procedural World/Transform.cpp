#include "Transform.h"

#include <iostream>

#include "..\glm\gtx\rotate_vector.inl"
#include "..\glm\gtc\matrix_transform.inl"
#include "..\glm\gtx\euler_angles.inl"

using namespace std;

Transform::Transform() : matrix( 1.0f )
{}

Transform::~Transform()
{}

void Transform::setPosition( const glm::vec3 & position )
{
	matrix = glm::translate( glm::mat4( 1.0f ), position ) * glm::scale( glm::mat4( 1.0f ), getScale() ) * glm::mat4_cast( getRotation() );
}

void Transform::setAngles( const glm::vec3 & angles )
{
	matrix = glm::translate( glm::mat4( 1.0f ), getPosition() ) * glm::scale( glm::mat4( 1.0f ), getScale() ) * glm::eulerAngleYXZ( angles.x, angles.y, angles.z );
}

void Transform::setRotation( const glm::quat & rotation )
{
	matrix = glm::translate( glm::mat4( 1.0f ), getPosition() ) * glm::scale( glm::mat4( 1.0f ), getScale() ) * glm::mat4_cast( rotation );
}

void Transform::setScale( const glm::vec3 & scale )
{
	matrix = glm::translate( glm::mat4( 1.0f ), getPosition() ) * glm::scale( glm::mat4( 1.0f ), scale ) * glm::mat4_cast( getRotation() );
}

void Transform::setMatrix( const glm::mat4 & matrix )
{
	this->matrix = matrix;
}

glm::vec3 Transform::getPosition() const
{
	return glm::vec3( (matrix * glm::vec4( 0.0f, 0.0f, 0.0f, 1.0f )) );
}

glm::quat Transform::getRotation() const
{
	return glm::rotation( glm::vec3( 0.0f, 0.0f, 1.0f ), glm::vec3( (matrix * glm::vec4( 0.0f, 0.0f, 1.0f, 0.0f )) ) );
}

glm::vec3 Transform::getAngles() const
{
	return glm::eulerAngles( getRotation() );
}

glm::vec3 Transform::getScale() const
{
	float x = glm::length( glm::vec3( (matrix * glm::vec4( 1.0f, 0.0f, 0.0f, 1.0f )) ) );
	float y = glm::length( glm::vec3( (matrix * glm::vec4( 0.0f, 1.0f, 0.0f, 1.0f )) ) );
	float z = glm::length( glm::vec3( (matrix * glm::vec4( 0.0f, 0.0f, 1.0f, 1.0f )) ) );
	return glm::vec3( x, y, z );
}

glm::mat4 Transform::getMatrix() const
{
	return glm::mat4();
}

void Transform::lookAt( glm::vec3 & target, glm::vec3 up )
{
	glm::vec3 position = getPosition();

	glm::quat rotation = glm::rotation( glm::vec3( 0.0f, 0.0f, 1.0f ), glm::normalize( target - position ) );

	matrix = glm::translate( glm::mat4( 1.0f ), position ) * glm::scale( glm::mat4( 1.0f ), getScale() ) *  glm::mat4_cast( glm::rotation( glm::vec3( 0.0f, 0.0f, 1.0f ), glm::normalize( target - position ) ) );
}

void Transform::translate( glm::vec3 & translation )
{
	setPosition( getPosition() + translation );
}

void Transform::rotate( glm::quat & rotation )
{
	setRotation( getRotation() * rotation );
}
