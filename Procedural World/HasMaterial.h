#pragma once

#include "Material.h"

using namespace std;

class HasMaterial
{
public:
	virtual ~HasMaterial();

	Material* getMesh() const;
	void setMesh( Material* mesh );

private:
	HasMaterial();

	Material* material;
};