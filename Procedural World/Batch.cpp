#include "Batch.h"

#include <iostream>

using namespace std;

// as defined by https://www.opengl.org/wiki/GLAPI/glDrawArraysIndirect
struct DrawArraysIndirectCommand
{
	unsigned int count;
	unsigned int instanceCount;
	unsigned int first;
	unsigned int baseInstance;
};

Batch::Batch( vector<Instance*>* instances )
{
	cout << "Batch()\n";

	//pack material properties
	//see http://www.openglsuperbible.com/2013/10/16/the-road-to-one-million-draws/
}

Batch::~Batch()
{
	cout << "~Batch()\n";
}
