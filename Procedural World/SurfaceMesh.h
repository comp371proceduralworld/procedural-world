#pragma once

#include "SubdivisionSurface.h"
#include "Mesh.h"

class SurfaceMesh
{
public:
	static void CalculateNormalsFor( SubdivisionSurface * surface );

	static Mesh * CreateMeshFrom( SubdivisionSurface * surface );

private:
	SurfaceMesh();
	~SurfaceMesh();
};

