#pragma once

#include "SubdivisionSurface.h"

class CatmullClark
{
public:
	static SubdivisionSurface * Subdivide( SubdivisionSurface * surface );
};