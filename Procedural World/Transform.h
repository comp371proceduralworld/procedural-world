#pragma once

#include "..\glm\glm.hpp"
#include "..\glm\gtx\quaternion.hpp"

class Transform
{
public:
	Transform();
	~Transform();

	void setPosition( const glm::vec3& position );
	void setAngles( const glm::vec3& angles );
	void setRotation( const glm::quat& rotation );
	void setScale( const glm::vec3& scale );
	void setMatrix( const glm::mat4& matrix );

	glm::vec3 getPosition() const;
	glm::quat getRotation() const;
	glm::vec3 getAngles() const;
	glm::vec3 getScale() const;
	glm::mat4 getMatrix() const;

	void lookAt( glm::vec3& target, glm::vec3 up );
	void translate( glm::vec3& translation );
	void rotate( glm::quat& rotation );

private:
	glm::mat4 matrix;
};

