#pragma once

#include <glew.h>
#include <glfw3.h>

GLFWwindow * createWindow( const int& window_width, const int& window_height, const char * window_title, int cursor_mode );

GLuint createShader( GLenum shader_type, const char* chars );

GLuint createProgram( const char* vertex_shader_filename, const char* fragment_shader_filename );

static GLuint vao;

void beginVAO();

void modifyVAO( GLuint id );

GLuint endVAO();

GLuint createEBO( const unsigned int& number_of_indices, const unsigned int * indices );

GLuint createVBO( const unsigned int& number_of_entries, const float * entries, const unsigned int& attribute_layout_location, const unsigned int& entries_per_attribute, GLboolean normalized );