WASD - translate camera
Arrow Keys - rotate model

All .png files were created by Oliver Philbin-Briscoe and Samuel Beland Leblanc.

For code and algorithm references, see report / slides.