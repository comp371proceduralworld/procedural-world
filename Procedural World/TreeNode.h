#pragma once

#include <glm.hpp>
#include "gtc/constants.hpp"

#include "CatmullClark.h"

class InterNode;

struct NodeBase
{
	vec3 position;

	vec3 initial;

	vec3 normal;
	vec3 tangent;
	vec3 bitangent;

	float radius;
	float length;

	const int sides;

	InterNode * predecessor;

	NodeBase( InterNode * predecessor, const vec3 & p, const vec3 & t, const vec3 & b, const vec3 & n, const int & s, const float & r, const float & l ) : predecessor( predecessor ), position( p ), tangent( t ), bitangent( b ), normal( n ), sides( s ), radius( r ), length( l ), initial( t ) {}
	~NodeBase() {}
};

struct InterNode : NodeBase
{
	int age;

	Vert ** loop;

	InterNode * successor;

	InterNode( SubdivisionSurface * surface, InterNode * predecessor, const vec3 & p, const vec3 & t, const vec3 & b, const vec3 & n, const int & s, const int & a, const float & r, const float & l, const float & v ) : NodeBase( predecessor, p, t, b, n, s, r, l ), age( a ), loop( new Vert *[s] )
	{
		float vert_radius = radius / cos( glm::pi<float>() / sides );

		float delta = (2.0f * glm::pi<float>()) / sides;
		
		float theta = - delta / 2.0f;

		for ( int i = 0; i < sides; i++, theta += delta )
		{
			loop[i] = new Vert( surface, position + (normal * cos( theta ) + bitangent * sin( theta )) * vert_radius );
			loop[i]->uv = vec2( i / (sides - 1.0f), v );
			loop[i]->tangent = tangent;
		}
	}

	~InterNode()
	{
		delete[] loop;
	}
};

struct BudNode : NodeBase
{
	int i, d, b;

	float interpolator;

	vec3 acceleration;

	float v;

	BudNode( InterNode * predecessor, const int & i, const vec3 & p, const vec3 & t, const vec3 & b, const vec3 & n, const int & s, const float & r, const float & l, const int & d, const float & v ) : NodeBase( predecessor, p, t, b, n, s, r, l ), i( i ), d( d ), b( 0 ), v( v ) {}
	~BudNode() {}
};