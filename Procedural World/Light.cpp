#include "Light.h"

#include <iostream>

using namespace std;

Light::Light()
{}


Light::~Light()
{}

ShadowCastingLight::~ShadowCastingLight()
{}

void ShadowCastingLight::enableShadows()
{}

void ShadowCastingLight::disableShadows()
{}

bool ShadowCastingLight::castsShadows()
{
	return false;
}

ShadowCastingLight::ShadowCastingLight()
{}

AmbientLight::AmbientLight()
{
	cout << "AmbientLight()\n";
}

AmbientLight::~AmbientLight()
{
	cout << "~AmbientLight()\n";
}

DirectionalLight::DirectionalLight()
{
	cout << "~DirectionalLight()\n";

	direction = glm::vec3( 0.0f, -1.0f, 0.0f );
}

DirectionalLight::~DirectionalLight()
{
	cout << "~DirectionalLight()\n";
}

void DirectionalLight::setDirection( glm::vec3 vector )
{
	direction = vector;
}

glm::vec3 DirectionalLight::getDirection()
{
	return direction;
}

PointLight::PointLight()
{
	cout << "PointLight()\n";
}

PointLight::~PointLight()
{
	cout << "~PointLight()\n";
}
