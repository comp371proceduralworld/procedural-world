#include "GLUtil.h"

#include <iostream>

#include "IOUtil.h"

GLFWwindow * createWindow( const int& window_width, const int& window_height, const char * window_title, int cursor_mode )
{
	GLFWwindow* window;

	//set appropriate hints
	glfwWindowHint( GLFW_SAMPLES, 4 );
	glfwWindowHint( GLFW_CONTEXT_VERSION_MAJOR, 3 );
	glfwWindowHint( GLFW_CONTEXT_VERSION_MINOR, 3 );
	glfwWindowHint( GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE );

	//create a GLFW window
	window = glfwCreateWindow( window_width, window_height, window_title, NULL, NULL );

	if ( window == NULL )
	{
		fprintf( stderr, "ERROR: could not open GLFW window.\n" );
		getchar();
		glfwTerminate();
		exit( 1 );
	}

	//set window as current context
	glfwMakeContextCurrent( window );

	//enable support for mouse controls
	glfwSetInputMode( window, GLFW_CURSOR, cursor_mode );

	return window;
}

GLuint createShader( GLenum shader_type, const char* chars )
{
	GLuint shader = glCreateShader( shader_type );

	glShaderSource( shader, 1, &chars, NULL );
	glCompileShader( shader );

	GLint gl_compule_status;

	glGetShaderiv( shader, GL_COMPILE_STATUS, &gl_compule_status );

	if ( gl_compule_status == GL_FALSE )
	{
		fprintf( stdout, "ERROR: shader compile failed." );
		std::cin.get();
		glfwTerminate();
		exit( EXIT_FAILURE );
	}

	return shader;
}

GLuint createProgram( const char* vertex_shader_filename, const char* fragment_shader_filename )
{
	GLuint program = glCreateProgram();

	GLuint vertex_shader_ID = createShader( GL_VERTEX_SHADER, readFileContents( vertex_shader_filename ) );
	GLuint fragment_shader_ID = createShader( GL_FRAGMENT_SHADER, readFileContents( fragment_shader_filename ) );

	//attach shader objects
	glAttachShader( program, vertex_shader_ID );
	glAttachShader( program, fragment_shader_ID );

	//link
	glLinkProgram( program );

	GLint gl_link_status;

	glGetProgramiv( program, GL_LINK_STATUS, &gl_link_status );

	if ( gl_link_status == GL_FALSE )
	{
		fprintf( stdout, "ERROR: could not link shaders." );
		std::cin.get();
		glfwTerminate();
		exit( EXIT_FAILURE );
	}

	//detach shader objects
	glDetachShader( program, vertex_shader_ID );
	glDetachShader( program, fragment_shader_ID );

	//cleanup shader objects
	glDeleteShader( vertex_shader_ID );
	glDeleteShader( fragment_shader_ID );

	return program;
}

void beginVAO()
{
	//create and bind vao
	glGenVertexArrays( 1, &vao );
	glBindVertexArray( vao );

	std::cout << "begin vao " << vao << '\n';
}

void modifyVAO( GLuint id )
{
	//re-bind vao
	glBindVertexArray( id );

	//track active vao
	vao = id;

	std::cout << "modify vao " << vao << '\n';
}

GLuint endVAO()
{
	if ( vao == 0 )
	{
		std::cout << "ERROR: cannot call endVAO() before beginVAO()\n";
		std::cin.get();
		exit( 1 );
	}

	std::cout << "end vao " << vao << '\n';

	//protect against unwanted modifications
	glBindVertexArray( 0 );;

	//store vertex array ID in temporary variable so that we can return it after zeroing the static member
	GLuint tmp = vao;

	vao = 0;

	return tmp;;
}

GLuint createEBO( const unsigned int & number_of_indices, const unsigned int * indices )
{
	if ( vao == 0 )
	{
		std::cout << "ERROR: cannot create an EBO outside of a begin/endVAO() block";
		std::cin.get();
		exit( 1 );
	}

	GLuint ebo;

	//create and bind ebo
	glGenBuffers( 1, &ebo );
	glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, ebo );

	//fill ebo
	glBufferData( GL_ELEMENT_ARRAY_BUFFER, number_of_indices, indices, GL_STATIC_DRAW );

	//protect against unwanted modifications
	glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, 0 );

	std::cout << "\tcreate ebo " << ebo << '\n';
}

GLuint createVBO( const unsigned int & number_of_entries, const float * entries, const unsigned int & attribute_layout_location, const unsigned int & entries_per_attribute, GLboolean normalized )
{
	if ( vao == 0 )
	{
		std::cout << "ERROR: cannot create a VBO outside of a begin/endVAO() block";
		std::cin.get();
		exit( 1 );
	}

	GLuint vbo;

	//create and bind vbo
	glGenBuffers( 1, &vbo );
	glBindBuffer( GL_ARRAY_BUFFER, vbo );

	//fill vbo
	glBufferData( GL_ARRAY_BUFFER, number_of_entries * sizeof( GLfloat ), entries, GL_STATIC_DRAW );

	//set attribute information
	glVertexAttribPointer( attribute_layout_location, 3, GL_FLOAT, normalized, entries_per_attribute * sizeof( GLfloat ), (GLvoid*) 0 );

	//protect against unwanted modifications
	glBindBuffer( GL_ARRAY_BUFFER, 0 );

	std::cout << "\tcreate vbo " << vbo << '\n';
}
