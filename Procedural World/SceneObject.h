#pragma once

#include <string>

using namespace std;

class SceneObject
{
public:
	SceneObject( const string& name = "" );
	virtual ~SceneObject();

	bool active;

	string name;
};

