#include "TreeGen.h"

#include "Random.h"

#include <glm.hpp>
#include "gtc/random.hpp"
#include "gtc/quaternion.hpp"
#include "gtx/quaternion.hpp"
#include "gtx/rotate_vector.hpp"
#include "gtc/matrix_transform.hpp"

#include "SurfaceMesh.h"

#include <ctime>

#include <iostream>

vec3 lerpv( const vec3 & a, const vec3 & b, float f )
{
	f = clamp( f, 0.0f, 1.0f );

	return a * (1.0f - f) + b * f;
}

float lerpf( const float & a, const float & b, float f )
{
	f = clamp( f, 0.0f, 1.0f );

	return a * (1.0f - f) + b * f;
}

TreeGen::TreeGen()
{}

TreeGen::~TreeGen()
{}

Mesh * TreeGen::Generate()
{
	GaussianDistribution phyllotactic_angle_d( phyllotactic_angle_min, phyllotactic_angle_max );
	GaussianDistribution branch_angle_d( branch_angle_min, branch_angle_max );
	GaussianDistribution velocity_continuity_d( velocity_continuity_min, velocity_continuity_max );
	GaussianDistribution acceleration_continuity_d( acceleration_continuity_min, acceleration_continuity_max );
	GaussianDistribution apical_share_d( apical_dominance, 1.0f );
	GaussianDistribution apical_control_d( 0.0f, 1.0f );

	SubdivisionSurface ** output = new SubdivisionSurface *[subdivisions + 1];

	// rough upper bounds, no guarantee that they won't be exceeded
	output[0] = new SubdivisionSurface( 5000, 10000, 8000 );

	int t = 0;

	vec3 tangent = dir;
	vec3 bitangent = cross( hint, tangent );
	vec3 normal = cross( tangent, bitangent );

	vector<BudNode *> apical_buds;

	float trunk_base_radius = 1.2f; // apical_diameter * pow( age, 1.0f / (apical_control) ) * (1.0f / (apical_dominance)) * 0.5f;

									//Debug.Log( "trunk_base_radius: " + trunk_base_radius );

	InterNode * root = new InterNode( output[0], nullptr, pos, tangent, bitangent, normal, sides, age, trunk_base_radius, trunk_base_radius * 2.0f, 0.0f );

	Vert * mid = new Vert( output[0], pos );

	for ( int i = 1; i < sides; i++ )
	{
		new Face( output[0], 3, new Vert * [3]{ root->loop[i], root->loop[i - 1], mid } );
	}

	new Face( output[0], 3, new Vert *[3]{ root->loop[0], root->loop[sides - 1], mid } );

	apical_buds.push_back( new BudNode( root, 0, pos, tangent, bitangent, normal, sides, trunk_base_radius, root->length, 0, 0.0f ) );

	while ( apical_buds.size() > 0 )
	{
		// "best" guess at ~# apical buds by end of growth cycle
		vector<BudNode *> apical_buds_next;

		apical_buds_next.reserve( (int) (apical_buds.size() + apical_buds.size() * branch_number * (1.0f - apical_control)) );

		for ( auto apical_bud : apical_buds )
		{
			if ( apical_bud->radius > halt_diameter / 2.0f )
			{
				// limit # branches to # sides
				int potential_lateral_buds = apical_bud->b > segment_limit ? 1 : min( apical_bud->sides, branch_number );

				// index delta / branch
				int delta_i = apical_bud->sides / potential_lateral_buds;

				// quick lookup
				bool * mask = new bool[apical_bud->sides];

				for ( int i = 0; i < apical_bud->sides; i++ )
				{
					mask[i] = false;
				}

				// theta
				float gamma = 0.0f;

				// theta gamma / branch
				float unit_gamma = (glm::pi<float>() * 2.0f / apical_bud->sides) * delta_i;

				vector<int> indices;
				vector<vec3> radial_vectors;
				vector<float> thetas;

				indices.reserve( potential_lateral_buds );
				radial_vectors.reserve( potential_lateral_buds );
				thetas.reserve( potential_lateral_buds );

				if ( (apical_bud->position.y > height_coefficient && apical_bud->d < depth_limit && apical_bud->b >= apical_bud->d * (2.0f)) || apical_bud->b > segment_limit )
				{
					// figure out how many lateral buds there will be
					for ( int i = 0; i < potential_lateral_buds; i += delta_i, gamma += unit_gamma )
					{
						if ( (apical_control_d.Sample() / apical_bud->d > lerpf( apical_control, 0.0f, 1.0f - (trunk_base_radius - apical_bud->radius) / trunk_base_radius )) || apical_bud->b > segment_limit )
						{
							indices.push_back( i );
							radial_vectors.push_back( (apical_bud->normal * cos( gamma ) + apical_bud->bitangent * sin( gamma )) * apical_bud->radius );
							thetas.push_back( gamma );
							mask[i] = true;
						}
					}
				}

				float * radii = new float[indices.size()];

				//Debug.Log( "indices.size(): " + indices.size() );

				if ( indices.size() > 0 )
				{
					// apical radius
					float ar_exp = pow( apical_diameter / 2.0f, da_vinci_exponent );

					// total radius 
					float radius_pool = pow( apical_bud->radius, da_vinci_exponent );

					// reserve a minimum of the apical radius for the lateral buds and successive apical bud
					float radius_reserved = (ar_exp * (indices.size() + 1)) / 2.0f;

					radius_pool -= radius_reserved;

					// apical bud goes first
					float apical_share = radius_pool * apical_share_d.Sample();

					radius_pool -= apical_share;

					apical_bud->radius = pow( ar_exp + apical_share, 1.0f / da_vinci_exponent ) * c_radial_decay;
					apical_bud->length = lerpf( apical_bud->radius * 2.0f, apical_bud->length, c_length_persistence );

					// lateral buds compete for the rest
					for ( int i = 0; i < indices.size() - 1; i++ )
					{
						if ( radius_pool > 0.0f )
						{
							int m = indices.size() - i;

							float mean = (radius_pool / m) / 2.0f;
							float std = mean;

							float lateral_share = apical_bud->b > segment_limit ? 0.5f : max( min( radius_pool, mean + gaussRand( 0.5f, 0.5f ) * std ), 0.0f );

							radius_pool -= lateral_share;

							radii[i] = pow( ar_exp + lateral_share, 1.0f / da_vinci_exponent ) * c_radial_decay;
						}
						else
						{
							radii[i] = (apical_diameter / 2.0f) * c_radial_decay;
						}
					}

					// the last lateral bud gets whatever's left
					radii[indices.size() - 1] = pow( ar_exp + radius_pool, 1.0f / da_vinci_exponent );
				}
				else
				{
					// apply radial decay
					apical_bud->radius *= c_radial_decay;
				}

				apical_bud->b++;

				//CALCULATE & APPLY CORRECTION VECTOR

				vec3 correction_vector = vec3( 0.0f );

				// correction vector points away from lateral buds
				for ( auto radial_vector : radial_vectors )
				{
					correction_vector -= radial_vector;
				}

				correction_vector = normalize( correction_vector );

				apical_bud->interpolator = (apical_bud->radius - halt_diameter / 2.0f) / (trunk_base_radius - halt_diameter / 2.0f);

				//CALCULATE & APPLY GROWTH VECTOR, PHYOLLOTACTIC ROTATION

				// get growth vector
				vec3 growth_vector = GrowthVector( apical_bud, c_gravitropic, c_historical, c_velocity, velocity_continuity_d, acceleration_continuity_d  );

				vec3 accumulator = vec3( 0.0f );

				int w = 0;

				for ( auto bud : apical_buds )
				{
					vec3 delta = apical_bud->position - bud->position;

					if ( bud->position == vec3( 0.0f ) )
					{
						continue;
					}

					float distance = length( delta );

					if ( distance > bud->radius * 2.0f )
					{
						accumulator += delta / 2.0f;
						w++;
					}
				}

				if ( w > 0 )
				{
					accumulator /= w;

					growth_vector = normalize( lerpv( growth_vector, accumulator, 1.0f - length( accumulator ) ) );
				}

				growth_vector = normalize( lerpv( growth_vector, up, (1.0f - (apical_bud->position.y / 6.0f)) * 0.5f ) );

				// get rotation
				
				mat4 growth_rotation = rotate( mat4( 1.0f ), degrees( phyllotactic_angle_d.Sample() ), growth_vector ) * glm::mat4_cast( rotation( apical_bud->tangent, growth_vector ) );

				// apply growth vector, scaled to bud's diameter
				apical_bud->position += growth_vector * apical_bud->length;

				// apply rotation
				apical_bud->tangent = vec3( growth_rotation * vec4( apical_bud->tangent, 0.0f ) );
				apical_bud->normal = vec3( growth_rotation * vec4( apical_bud->normal, 0.0f ) );
				apical_bud->bitangent = vec3( growth_rotation * vec4( apical_bud->bitangent, 0.0f ) );

				apical_bud->v += apical_bud->length;

				int current_age = age - t;

				// create internode
				InterNode * apical_node = apical_bud->predecessor = new InterNode( output[0], apical_bud->predecessor, apical_bud->position, apical_bud->tangent, apical_bud->bitangent, apical_bud->normal, apical_bud->sides, current_age, apical_bud->radius, apical_bud->length, apical_bud->v );

				apical_node->predecessor->successor = apical_node;

				// FILL GEOMETRY

				for ( int i = 0, j = 0; i < apical_node->sides; i++ )
				{
					int next_i = i == apical_node->sides - 1 ? 0 : i + 1;

					// create lateral node
					if ( mask[i] )
					{
						if ( radii[j] > halt_diameter * (1.0f / c_radial_decay) * 0.75f )
						{
							vec3 radial_vector = apical_node->normal * cos( thetas[j] ) + apical_node->bitangent * sin( thetas[j] );

							vec3 lateral_bitangent = cross( radial_vector, growth_vector );

							float c = branch_angle_d.Sample();

							vec3 lateral_tangent = rotate( apical_bud->tangent, degrees( - c ), lateral_bitangent );
							vec3 lateral_normal = cross( lateral_tangent, lateral_bitangent );

							vec3 position = apical_node->position + radial_vector * apical_node->radius + lateral_tangent * radii[j] / 2.0f + lateral_normal * radii[j];

							InterNode * lateral_node = new InterNode( output[0], apical_node->predecessor, position, lateral_tangent, lateral_bitangent, lateral_normal, 4, current_age, radii[j], lerpf( radii[j] * 2.0f, apical_node->predecessor->length, c_length_persistence ), apical_bud->v );

							apical_buds_next.push_back( new BudNode( lateral_node, indices[j], position, lateral_tangent, lateral_bitangent, lateral_normal, 4, radii[j], lateral_node->length, apical_bud->d + 1, apical_bud->v ) );

							apical_buds_next[apical_buds_next.size() - 1]->acceleration = lateral_node->tangent;

							// AS IF FACING TREE (standing along radial_vector, facing down negative radial_vector)

							// bottom
							new Face( output[0], 4, new Vert *[4]{ apical_node->predecessor->loop[i], apical_node->predecessor->loop[next_i], lateral_node->loop[1], lateral_node->loop[0] } );

							// top
							new Face( output[0], 4, new Vert *[4]{ lateral_node->loop[3], lateral_node->loop[2], apical_node->loop[next_i], apical_node->loop[i] } );

							// left
							new Face( output[0], 4, new Vert *[4]{ apical_node->predecessor->loop[i], lateral_node->loop[0], lateral_node->loop[3], apical_node->loop[i] } );

							// right
							new Face( output[0], 4, new Vert *[4]{ lateral_node->loop[1], apical_node->predecessor->loop[next_i], apical_node->loop[next_i], lateral_node->loop[2] } );
						}
						else
						{
							new Face( output[0], 4, new Vert *[4]{ apical_node->predecessor->loop[i], apical_node->predecessor->loop[next_i], apical_node->loop[next_i], apical_node->loop[i] } );
						}

						j++;
					}

					// create face
					else
					{
						new Face( output[0], 4, new Vert *[4]{ apical_node->predecessor->loop[i], apical_node->predecessor->loop[next_i], apical_node->loop[next_i], apical_node->loop[i] } );
					}
				}

				apical_buds_next.push_back( apical_bud );
			}

			// CAP BRANCH

			Vert * tip = new Vert( output[0], apical_bud->predecessor->position );

			output[0]->verts.push_back( tip );

			for ( int i = 1; i < apical_bud->sides; i++ )
			{
				new Face( output[0], 3, new Vert *[3]{ tip, apical_bud->predecessor->loop[i - 1], apical_bud->predecessor->loop[i] } );
			}

			new Face( output[0], 3, new Vert *[3]{ tip, apical_bud->predecessor->loop[apical_bud->sides - 1], apical_bud->predecessor->loop[0] } );
		}

		apical_buds = apical_buds_next;

		cout << "growth cycle..." << endl;
	}

	cout << "input verts: " + output[0]->verts.size() << endl;
	cout << "input edges: " + output[0]->edges.size() << endl;
	cout << "input faces: " + output[0]->faces.size() << endl;

	for ( int i = 1; i < subdivisions + 1; i++ )
	{
		output[i] = CatmullClark::Subdivide( output[i - 1] );
	}

	cout << "output verts: " + output[subdivisions]->verts.size() << endl;
	cout << "output edges: " + output[subdivisions]->edges.size() << endl;
	cout << "output faces: " + output[subdivisions]->faces.size() << endl;

	SurfaceMesh::CalculateNormalsFor( output[subdivisions] );

	return SurfaceMesh::CreateMeshFrom( output[subdivisions] );
}

vec3 TreeGen::GravitropicVector( BudNode * bud, float gspa )
{
	if ( gspa > 0.0f )
	{
		vec3 axis;

		if ( bud->tangent == up )
		{
			axis = sphericalRand( 1.0f );
			axis = normalize( vec3( axis.x, 0.0f, axis.y ) );
		}
		else
		{
			axis = normalize( cross( bud->tangent, up ) );
		}

		return rotate( up, degrees( gspa ), axis );
	}

	return up;
}

vec3 TreeGen::ApicalVector( BudNode * bud, GaussianDistribution & velocity_continuity_d, GaussianDistribution & acceleration_continuity_d )
{
	float c = velocity_continuity_d.Sample();

	vec3 vector = normalize( lerpv( sphericalRand( 1.0f ), bud->tangent, c ) );

	bud->acceleration = normalize( lerpv( sphericalRand( 1.0f ), bud->acceleration, acceleration_continuity_d.Sample() ) );

	return normalize(vector + bud->acceleration * c_acceleration);
}

vec3 TreeGen::GrowthVector( BudNode * bud, float c_gravitopic, float c_historical, float c_velocity, GaussianDistribution & velocity_continuity_d, GaussianDistribution & acceleration_continuity_d )
{
	vec3 historical = bud->initial;
	vec3 gravitropic = GravitropicVector( bud, pi<float>() - abs( lerpf( -pi<float>(), pi<float>(), bud->interpolator ) ) );
	vec3 apical = ApicalVector( bud, velocity_continuity_d, acceleration_continuity_d );

	return normalize(apical * c_velocity + gravitropic * c_gravitopic * (1.0f - bud->interpolator) + historical * c_historical);
}
