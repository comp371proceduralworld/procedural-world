#include "CatmullClark.h"

// interpolators for vertices in 3 faces
const float m1_3 = 0.0f;
const float m2_3 = 1.0f / 3.0f;
const float m3_3 = 2.0f / 3.0f;

// interpolators for vertices in 4 faces
const float m1_4 = 0.25f;
const float m2_4 = 0.25f;
const float m3_4 = 0.5f;

SubdivisionSurface * CatmullClark::Subdivide( SubdivisionSurface * surface )
{
	SubdivisionSurface * subdivision = new SubdivisionSurface( surface );

	for ( auto edge : surface->edges )
	{
		edge->point = new Vert( subdivision, (edge->center + (edge->faces[0]->position + edge->faces[1]->position) / 2.0f) / 2.0f, (edge->a->uv + edge->b->uv) / 2.0f, (edge->a->tangent + edge->b->tangent) / 2.0f );
	}

	for ( auto vertex : surface->verts )
	{
		vec3 face_point_average = vec3( 0.0f );

		for ( auto adjacency : vertex->adjacencies )
		{
			face_point_average += adjacency.face->position;
		}

		face_point_average /= vertex->adjacencies.size();

		vec3 edge_center_average = vertex->edge_center_sum / (float) vertex->edges;

		if ( vertex->adjacencies.size() == 3 )
		{
			vertex->descendant = new Vert( subdivision, m1_3 * vertex->position + m2_3 * face_point_average + m3_3 * edge_center_average, vertex->uv, vertex->tangent );
		}

		else // assume quad
		{
			vertex->descendant = new Vert( subdivision, m1_4 * vertex->position + m2_4 * face_point_average + m3_4 * edge_center_average, vertex->uv, vertex->tangent );
		}
	}

	for ( auto face : surface->faces )
	{
		face->point = new Vert( subdivision, face->position, face->uv, face->tangent );

		if ( face->sides == 3 )
		{
			new Face( subdivision, 4, new Vert* [4] { face->vertices[0]->descendant, face->edges[0]->point, face->point, face->edges[2]->point } );
			new Face( subdivision, 4, new Vert* [4] { face->vertices[1]->descendant, face->edges[1]->point, face->point, face->edges[0]->point } );
			new Face( subdivision, 4, new Vert* [4] { face->vertices[2]->descendant, face->edges[2]->point, face->point, face->edges[1]->point } );
		}

		else // assume quad
		{
			new Face( subdivision, 4, new Vert* [4] { face->vertices[0]->descendant, face->edges[0]->point, face->point, face->edges[3]->point } );
			new Face( subdivision, 4, new Vert* [4] { face->vertices[1]->descendant, face->edges[1]->point, face->point, face->edges[0]->point } );
			new Face( subdivision, 4, new Vert* [4] { face->vertices[2]->descendant, face->edges[2]->point, face->point, face->edges[1]->point } );
			new Face( subdivision, 4, new Vert* [4] { face->vertices[3]->descendant, face->edges[3]->point, face->point, face->edges[2]->point } );
		}
	}

	return subdivision;
}
