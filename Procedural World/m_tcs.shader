#version 430 core

layout(vertices = 3) out;

in vec3 vOriPos[];
out vec3 tcsOriPos[];

in vec3 vNormal[];
out vec3 tcsNormal[];

in vec2 vUV[];
out vec2 tcsUV[];

void main(void) {
	if (gl_InvocationID == 0) {
		gl_TessLevelInner[0] = 70.0;
		gl_TessLevelOuter[0] = 70.0;
		gl_TessLevelOuter[1] = 70.0;
		gl_TessLevelOuter[2] = 70.0;
	}
	gl_out[gl_InvocationID].gl_Position = gl_in[gl_InvocationID].gl_Position;
	tcsOriPos[gl_InvocationID] = vOriPos[gl_InvocationID];
	tcsNormal[gl_InvocationID] = vNormal[gl_InvocationID];
	tcsUV[gl_InvocationID] = vUV[gl_InvocationID];
}