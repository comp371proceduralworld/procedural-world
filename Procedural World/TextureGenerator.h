#pragma once
#include "glm.hpp"

class TextureGenerator
{
public:
	TextureGenerator(int width, int height, float horizontalFactor, float verticalFactor, float turbPower, int edgeOverlap);
	~TextureGenerator();

	int GetHeight() const { return _height; };
	int GetWidth() const { return _width; };
	glm::vec3* const GetDataPtr() const { return _data; };
	glm::vec3* const GetNormalsPtr() const { return _normals; };

private:
	int _height;
	int _width;
	glm::vec3* _data;
	glm::vec3* _normals;

};

