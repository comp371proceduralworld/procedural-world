#pragma once

class ArrayHelper {
public:
	template <class T>
	static T** Generate2DArray(int width, int height) {
		T** temp = new T*[height];
		for (int i = 0; i < height; ++i) {
			temp[i] = new T[width];
		}
		return temp;
	}

	template <class T>
	static void Delete2DArray(T** data, int height) {
		for (int i = 0; i < height; ++i) {
			delete[] data[i];
		}
		delete[] data;
	}

};