#include "Mesh.h"

#include <iostream>

#include "GLUtil.h"

using namespace std;

Mesh::Mesh( const unsigned int & vertices, const unsigned int & triangles ) : vertices( vertices ), triangles( triangles )
{
	cout << "Mesh()\n";

	position_data = new float[vertices * 3];
	triangle_data = new unsigned int[triangles * 3];
}

void Mesh::setVertex( int i, glm::vec3 position )
{
	i *= 3;
	position_data[i++] = position.x;
	position_data[i++] = position.y;
	position_data[i]   = position.z;
}

glm::vec3 Mesh::getVertex( int i ) const
{
	i *= 3;
	return glm::vec3( position_data[i++], position_data[i++], position_data[i] );
}

void Mesh::setTriangle( int i, unsigned int a, unsigned int b, unsigned int c )
{
	i *= 3;
	triangle_data[i++] = a;
	triangle_data[i++] = b;
	triangle_data[i]   = c;
}

std::tuple<unsigned int, unsigned int, unsigned int> Mesh::getTriangle( int i ) const
{
	i *= 3;
	return std::tuple<unsigned int, unsigned int, unsigned int>( triangle_data[i++], triangle_data[i++], triangle_data[i] );
}

void Mesh::setColor( int i, glm::vec3 color )
{
	i *= 4;
	color_data[i++] = color.r;
	color_data[i++] = color.g;
	color_data[i] = color.b;
}

glm::vec3 Mesh::getColor( int i ) const
{
	i *= 4;
	return glm::vec3( color_data[i++], color_data[i++], color_data[i] );
}

void Mesh::setNormal( int i, glm::vec3 vector )
{
	i *= 3;
	normal_data[i++] = vector.x;
	normal_data[i++] = vector.y;
	normal_data[i]   = vector.z;
}

glm::vec3 Mesh::getNormal( int i ) const
{
	i *= 3;
	return glm::vec3( normal_data[i++], normal_data[i++], normal_data[i] );
}

void Mesh::setTangent( int i, glm::vec3 vector )
{
	i *= 4;
	tangent_data[i++] = vector.x;
	tangent_data[i++] = vector.y;
	tangent_data[i] = vector.z;
}

glm::vec3 Mesh::getTangent( int i ) const
{
	i *= 4;
	return glm::vec3( tangent_data[i++], tangent_data[i++], tangent_data[i] );
}

void Mesh::setBitangent( int i, glm::vec3 vector )
{
	i *= 4;
	bitangent_data[i++] = vector.x;
	bitangent_data[i++] = vector.y;
	bitangent_data[i] = vector.z;
}

glm::vec3 Mesh::getBitangent( int i ) const
{
	i *= 4;
	return glm::vec3( bitangent_data[i++], bitangent_data[i++], bitangent_data[i] );
}

void Mesh::setUV( int i, glm::vec2 coordinate )
{
	i *= 2;
	uv_data[i++] = coordinate.x;
	uv_data[i++] = coordinate.y;
}

glm::vec2 Mesh::getUV( int i ) const
{
	i *= 2;
	return glm::vec2( uv_data[i++], uv_data[i++] );
}

unsigned int Mesh::getVertexCount() const
{
	return vertices;
}

unsigned int Mesh::getTriangleCount() const
{
	return triangles;
}

void Mesh::addColorData()
{
	if ( !color_data )
	{
		color_data = new float[vertices * 4];
	}
}

bool Mesh::hasColorData() const
{
	return color_data != nullptr;
}

void Mesh::addNormalData()
{
	if ( !normal_data )
	{
		normal_data = new float[vertices * 3];
	}
}

bool Mesh::hasNormalData() const
{
	return normal_data != nullptr;
}

void Mesh::addTangentData()
{
	if ( !tangent_data )
	{
		tangent_data = new float[vertices * 4];
	}
}

bool Mesh::hasTangentData() const
{
	return tangent_data != nullptr;
}

void Mesh::addBiangentData()
{
	if ( !bitangent_data )
	{
		bitangent_data = new float[vertices * 4];
	}
}

bool Mesh::hasBiangentData() const
{
	return bitangent_data != nullptr;
}

void Mesh::addUVData()
{
	if ( !uv_data )
	{
		uv_data = new float[vertices * 2];
	}
}

bool Mesh::hasUVData() const
{
	return uv_data != nullptr;
}

unsigned int * Mesh::getTriangleData() const
{
	return triangle_data;
}

float * Mesh::getVertexData() const
{
	return position_data;
}

float * Mesh::getColorData() const
{
	return color_data;
}

float * Mesh::getNormalData() const
{
	return normal_data;
}

float * Mesh::getTangentData() const
{
	return tangent_data;
}

float * Mesh::getBitangentData() const
{
	return bitangent_data;
}

float * Mesh::getUVData() const
{
	return uv_data;
}

Mesh::~Mesh()
{
	cout << "~Mesh()\n";

	delete position_data;
	delete triangle_data;

	if ( color_data )
	{
		delete color_data;
	}

	if ( normal_data )
	{
		delete normal_data;
	}

	if ( tangent_data )
	{
		delete tangent_data;
	}

	if ( bitangent_data )
	{
		delete bitangent_data;
	}

	if ( uv_data )
	{
		delete uv_data;
	}
}

void Mesh::push( GLuint vao_ID )
{
	pop();

	this->vao_ID = vao_ID;

	modifyVAO( vao_ID );

	ebo_ID = createEBO( triangles * 3, triangle_data );

	position_vbo_ID = createVBO( vertices * 3, position_data, 0, 3, GL_FALSE );

	if ( hasColorData() )
	{
		color_vbo_ID = createVBO( vertices * 3, color_data, 0, 3, GL_FALSE );
	}

	if ( hasNormalData() )
	{
		normal_vbo_ID = createVBO( vertices * 3, normal_data, 0, 3, GL_FALSE );
	}

	if ( hasTangentData() )
	{
		tangent_vbo_ID = createVBO( vertices * 3, tangent_data, 0, 3, GL_FALSE );
	}

	if ( hasBiangentData() )
	{
		bitangent_vbo_ID = createVBO( vertices * 3, bitangent_data, 0, 3, GL_FALSE );
	}

	if ( hasUVData() )
	{
		uv_vbo_ID = createVBO( vertices * 2, uv_data, 0, 2, GL_FALSE );
	}

	endVAO();
}

void Mesh::pop()
{
	if ( vao_ID != 0 )
	{
		modifyVAO( vao_ID );

		if ( ebo_ID != 0 )
		{
			glDeleteBuffers( 1, &ebo_ID );
		}

		if ( position_vbo_ID != 0 )
		{
			glDeleteBuffers( 1, &position_vbo_ID );
		}

		if ( color_vbo_ID != 0 )
		{
			glDeleteBuffers( 1, &position_vbo_ID );
		}

		if ( normal_vbo_ID != 0 )
		{
			glDeleteBuffers( 1, &normal_vbo_ID );
		}

		if ( tangent_vbo_ID != 0 )
		{
			glDeleteBuffers( 1, &tangent_vbo_ID );
		}

		if ( bitangent_vbo_ID != 0 )
		{
			glDeleteBuffers( 1, &bitangent_vbo_ID );
		}

		if ( uv_vbo_ID != 0 )
		{
			glDeleteBuffers( 1, &uv_vbo_ID );
		}

		endVAO();

		vao_ID = 0;

		ebo_ID = 0;
		position_vbo_ID = 0;
		color_vbo_ID = 0;
		normal_vbo_ID = 0;
		tangent_vbo_ID = 0;
		bitangent_vbo_ID = 0;
		uv_vbo_ID = 0;
	}
}

void Mesh::sub()
{
	if ( vao_ID != 0 )
	{
		modifyVAO( vao_ID );

		if ( position_vbo_ID != 0 )
		{
			glBufferSubData( position_vbo_ID, 0, sizeof( GLfloat ) * vertices * 3, position_data );
		}

		if ( color_vbo_ID != 0 )
		{
			glBufferSubData( color_vbo_ID, 0, sizeof( GLfloat ) * vertices * 3, color_data );
		}

		if ( normal_vbo_ID != 0 )
		{
			glBufferSubData( normal_vbo_ID, 0, sizeof( GLfloat ) * vertices * 3, normal_data );
		}

		if ( tangent_vbo_ID != 0 )
		{
			glBufferSubData( tangent_vbo_ID, 0, sizeof( GLfloat ) * vertices * 3, tangent_data );
		}

		if ( bitangent_vbo_ID != 0 )
		{
			glBufferSubData( bitangent_vbo_ID, 0, sizeof( GLfloat ) * vertices * 3, bitangent_data );
		}

		if ( uv_vbo_ID != 0 )
		{
			glBufferSubData( uv_vbo_ID, 0, sizeof( GLfloat ) * vertices * 2, uv_data );
		}

		endVAO();
	}
}

GLuint Mesh::getVertexBufferID() const
{
	return position_vbo_ID;
}

GLuint Mesh::getElementBufferID() const
{
	return ebo_ID;
}

GLuint Mesh::getColorBufferID() const
{
	return color_vbo_ID;
}

GLuint Mesh::getNormalBufferID() const
{
	return normal_vbo_ID;
}

GLuint Mesh::getTangentBufferID() const
{
	return tangent_vbo_ID;
}

GLuint Mesh::getUVBufferID() const
{
	return uv_vbo_ID;
}
