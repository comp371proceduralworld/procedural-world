#version 430 core
out vec4 color;

uniform sampler2D mossTexture;
uniform mat4 projection_matrix;
uniform mat4 view_matrix;


in vec2 uv;
in vec3 gsNormal;
in vec3 fragPos;

void main()
{
	vec4 colTemp = texture(mossTexture, uv);
	if (colTemp.a < 0.9) {
		discard;
	}

	vec3 lightColour = vec3(1.0f, 1.0f, 1.0f);
	vec3 cubeColour = colTemp.rgb;

	//ambient lighting
	float ambientStrength = 0.1f;
	vec3 ambient_contribution = ambientStrength * lightColour;

	//diffuse lighting
	vec3 light_position = vec3(0.0f, 0.0f, 30.0f); //world coords
	light_position = (projection_matrix * view_matrix * vec4(light_position, 1.0)).xyz;

	vec3 norm  = normalize(gsNormal);
	vec3 light_direction = normalize(light_position - fragPos);
	float incident_degree = max(dot(norm, light_direction), 0.0f);
	vec3 diffuse_contribution = incident_degree * lightColour;

	vec3 resultantColour = (ambient_contribution + diffuse_contribution) * cubeColour;


	color = vec4(resultantColour.xyz, 1.0);

	//color = colTemp;
}