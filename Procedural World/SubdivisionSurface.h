#pragma once

#include <vector>
#include "glm.hpp""

using namespace std;
using namespace glm;

class Vert;
class Edge;
class Face;

class SubdivisionSurface
{
public:

	SubdivisionSurface( SubdivisionSurface * surface )
	{
		verts.reserve( surface->verts.size() + surface->edges.size() + surface->faces.size() );
		edges.reserve( surface->edges.size() * 2 + surface->n_quad * 4 + surface->n_tris * 3 );
		faces.reserve( surface->n_quad * 4 + surface->n_tris * 3 );
	}

	SubdivisionSurface( int v, int e, int f )
	{
		verts.reserve( v );
		edges.reserve( e );
		faces.reserve( f );
	}

	~SubdivisionSurface()
	{
		for ( auto vertex : verts )
		{
			delete vertex;
		}

		for ( auto edge : edges )
		{
			delete edge;
		}

		for ( auto face : faces )
		{
			delete face;
		}
	}

	int n_tris;
	int n_quad;

	vector<Vert *> verts;
	vector<Edge *> edges;
	vector<Face *> faces;
};

struct Adjacency
{
	Vert * vertex;
	Edge * edge;
	Face * face;

	Adjacency() {}

	Adjacency( Vert * vertex, Edge * edge, Face * face ) : vertex( vertex ), edge( edge ), face( face ) {}
};

struct Vert
{
	vec3 position;
	vec3 normal;
	vec3 tangent;
	vec3 bitangent;
	vec2 uv;

	// directed edges, appened to in Face constructor
	vector<Adjacency> adjacencies;

	// incremented in Edge construtor
	int edges;

	// summed in Edge constructor
	vec3 edge_center_sum;

	// used by subdivision
	Vert * descendant;

	// used during Mesh conversion
	unsigned int order;

	Vert ( SubdivisionSurface * surface, vec3 position, int n = 4 ) : position( position )
	{
		surface->verts.push_back( this );
		adjacencies.reserve( n );
	}

	Vert ( SubdivisionSurface * surface, vec3 position, vec2 uv, vec3 tangent, int n = 4 ) : position( position ), uv( uv ), tangent( tangent )
	{
		surface->verts.push_back( this );
		adjacencies.reserve( n );
	}

	~Vert() {}

	bool TryGetAdjacency( Vert * vertex, Adjacency * & result )
	{
		for ( auto adjacency : adjacencies )
		{
			if ( adjacency.vertex == vertex )
			{
				result = &adjacency;
				return true;
			}
		}

		return false;
	}

	bool TryGetAdjacency( Edge * edge, Adjacency * & result )
	{
		for ( auto adjacency : adjacencies )
		{
			if ( adjacency.edge == edge )
			{
				result = &adjacency;
				return true;
			}
		}

		return false;
	}

	bool TryGetAdjacency( Face * face, Adjacency * & result )
	{
		for ( auto adjacency : adjacencies )
		{
			if ( adjacency.face == face )
			{
				result = &adjacency;
				return true;
			}
		}

		return false;
	}
};

struct Edge
{
	Vert * a, * b;

	Vert * point;

	vec3 center;

	vector<Face *> faces;

	Edge( Vert * a, Vert * b ) : a( a ), b( b ), center( (a->position + b->position) / 2.0f )
	{
		a->edges++;
		b->edges++;

		a->edge_center_sum += center;
		b->edge_center_sum += center;

		faces.reserve( 2 );
	}

	~Edge()	{}
};

struct Face
{
	int sides;

	Vert ** vertices;

	Edge ** edges;

	vec3 position;
	vec3 tangent;
	vec2 uv;

	Vert * point;

	Face( SubdivisionSurface * surface, int sides, Vert ** vertices ) : sides( sides ), vertices( vertices ), edges( new Edge *[sides] ), position( 0.0f ), uv( 0.0f )
	{
		for ( int i = 0; i < sides; i++ )
		{
			Adjacency * adjacency;

			int next = (i + 1) % sides;

			Vert * vertex = vertices[next];

			Edge * edge;

			if ( vertex->TryGetAdjacency( vertices[i], adjacency ) )
			{
				edge = adjacency->edge;
			}
			else
			{
				surface->edges.push_back( edge = new Edge( vertices[i], vertex ) );
			}

			edge->faces.push_back( this );
			vertices[i]->adjacencies.push_back( Adjacency( vertex, edge, this ) );
			edges[i] = edge;

			position += vertices[i]->position;
			uv += vertices[i]->uv;
			tangent += vertices[i]->tangent;
		}

		position /= sides;
		uv /= sides;
		tangent /= sides;

		surface->faces.push_back( this );

		if ( sides == 3 )
		{
			surface->n_tris++;
		}

		else // assume quad
		{
			surface->n_quad++;
		}
	}

	~Face()
	{
		delete[] vertices;
		delete[] edges;
	}
};

