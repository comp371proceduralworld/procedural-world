#pragma once

#include "Mesh.h"

class Primitives
{
	Primitives();
	~Primitives();

	static Mesh * Icosahedron( const unsigned int& subdivisions );

	static Mesh * Cube( const unsigned int& subdivisions );

	static Mesh * Plane( const unsigned int& subdivisions );
};
