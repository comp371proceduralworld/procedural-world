#pragma once

#include "TreeNode.h"
#include "Mesh.h"
#include "Random.h"

static const vec3 up = vec3( 0.0f, 1.0f, 0.0f );

class TreeGen
{
public:
	// millisecond allotment / frame
	int budget;

	// growth cycles
	int age;

	// trunk sides
	int sides;

	// initial position
	vec3 pos;

	// initial direction
	vec3 dir;

	// normal hint
	vec3 hint;

	// trunk sides
	int subdivisions;

	// roll
	float phyllotactic_angle_min;
	float phyllotactic_angle_max;

	// gravitropic set point angle, 0 = straight up, pi = straight down
	float gspa;

	// first-order straightness (UNUSED)
	float velocity_continuity_min;
	float velocity_continuity_max;

	// second-order branch straightness
	float acceleration_continuity_min;
	float acceleration_continuity_max;

	// influence of gravity
	float c_gravitropic;

	// influence of initial branch orientation on apical orientation
	float c_historical;

	// influence of velocity
	float c_velocity;

	// influence of acceleration
	float c_acceleration;

	// attenuation of lateral branch growth rate by parent branch
	float apical_dominance;

	// inhibiton of lateral bud flushing by parent branch
	float apical_control;

	// low angle ~= parallel, high angle ~= perpendicular
	float branch_angle_min;
	float branch_angle_max;

	// relates the diameter of an internode to the diameters of its children
	float da_vinci_exponent = 2.0f;

	// controls radius decay
	float c_radial_decay;

	// 0 ~= strictly based on diameter, 1 ~= strictly based on parent length
	float c_length_persistence;

	// branches per node
	int branch_number;

	// theoretical minimum diameter (at tip)
	float apical_diameter;

	// actual minimum diameter (after which no no shoots are grown)
	float halt_diameter;

	// hard branching depth limit
	int depth_limit = 2;

	// hard branching depth limit
	int segment_limit = 20;

	// min height of first branches
	float height_coefficient;

	// minimum diameter at branch base for subdivision approach
	float diameter_coefficient;

	TreeGen();
	~TreeGen();

	Mesh * Generate();

private:
	InterNode * root;

	vec3 GravitropicVector( BudNode * bud, float gspa );

	vec3 ApicalVector( BudNode * bud, GaussianDistribution & velocity_continuity_d, GaussianDistribution & acceleration_continuity_d );

	vec3 GrowthVector( BudNode * bud, float c_gravitopic, float c_historical, float c_velocity, GaussianDistribution & velocity_continuity_d, GaussianDistribution & acceleration_continuity_d );
};

