#include "Window.h"

#include "GLUtil.h"
#include <algorithm>

Window::Window() : Window( DEFAULT_WIDTH, DEFAULT_HEIGHT, "", false )
{}

Window::Window( int width, int height, const char * title, bool locked ) : width( width ), height( height ), locked( locked )
{
	//use the GL utiltiy header to create the new GLFW window that this Window instance will wrap
	window = createWindow( width, height, "", GLFW_CURSOR_NORMAL );

	//handle resize events
	glfwSetWindowSizeCallback( window, windowResizeCallback );

	//store pair in map to allow callbacks on instances
	windows.push_back( this );

	//cache aspect ratio
	aspectRatio = width / (float) height;
}

Window::~Window()
{
	windows.erase( std::remove( windows.begin(), windows.end(), this ), windows.end() );
}

int Window::getWidth() const
{
	return width;
}

int Window::getHeight() const
{
	return height;
}

float Window::getAspectRatio() const
{
	return aspectRatio;
}

void Window::setWidth( const int & width )
{
	glfwSetWindowSize( window, width, height );
}

void Window::setHeight( const int & height )
{
	glfwSetWindowSize( window, width, height );
}

void Window::setTitle( const char * title )
{
	glfwSetWindowTitle( window, title );
}

void Window::lock()
{
	locked = true;
}

void Window::unlock()
{
	locked = false;
}

void Window::showCursor()
{
	glfwSetInputMode( window, GLFW_CURSOR, GLFW_CURSOR_NORMAL );
}

void Window::hideCursor()
{
	glfwSetInputMode( window, GLFW_CURSOR, GLFW_CURSOR_HIDDEN );
}

void Window::setKeyCallback( GLFWkeyfun callback )
{
	glfwSetKeyCallback( window, callback );
}

void Window::setCursorPositionCallback( GLFWcursorposfun callback )
{
	glfwSetCursorPosCallback( window, callback );
}

void Window::setMouseButtonCallback( GLFWmousebuttonfun callback )
{
	glfwSetMouseButtonCallback( window, callback );
}

void Window::setScrollWheelCallback( GLFWscrollfun callback )
{
	glfwSetScrollCallback( window, callback );
}

void Window::swapBuffers()
{
	glfwSwapBuffers( window );
}

bool Window::shouldClose()
{
	return glfwWindowShouldClose( window );
}

void windowResizeCallback( GLFWwindow * window, int width, int height )
{
	for ( auto w : windows )
	{
		if ( w->window == window )
		{
			w->handleResize( width, height );
		}
	}
}

void Window::handleResize( int width, int height )
{
	if ( locked )
	{
		//restore previous width/height
		glfwSetWindowSize( window, this->width, this->height );
	}
	else
	{
		//cache new width/height
		this->width = width;
		this->height = height;

		//cache aspect ratio
		aspectRatio = width / (float) height;
	}
}
