#pragma once
class CanUpdate
{
public:
	~CanUpdate();
	virtual void update() = 0;

protected:
	CanUpdate();
};

